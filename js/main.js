/*global $ */
/*eslint-env browser*/

$(document).ready(function () {

    'use strict';
    function reslider(){
        if ($('.container').width() <= 720 ){
            $('.infoWidget').removeClass('wows fadeInRight fadeInLeft').removeAttr('data-wow-duration');
            $('.a_control-slider').show();
            $(".slider").slick({
                rtl: true,
                nextArrow:".a_arrow.a_next",
                prevArrow:".a_arrow.a_prev",
                infinite:false
            });
        }
     };
    reslider();
    $(window).resize(function(){     
        reslider();
    }); 
     var owl = $('.homeSlider .owl-carousel');
    owl.owlCarousel({
        loop:true,
        nav:true,
        dots:false,
        margin:10,
        items:1,
        rtl:true,
        autoplay:true,
        autoplayTimeout:8000,
        smartSpeed:550,
        autoplayHoverPause:false,
        responsiveClass:true, 
    });
    var owl2 = $('.sliderTeam');
    owl2.owlCarousel({
        loop:false,
        nav:true,
        dots:true,
        margin:10,
        items:3,
        rtl:true,
        autoplay:false,
        smartSpeed:550,
        responsiveClass:true, 
        responsive : {
            // breakpoint from 0 up
            0 : {
                items:1
            },
            // breakpoint from 480 up
            480 : {
                items:1
            },
            // breakpoint from 768 up
            768 : {
                items:2
            },
            1200 :{
                items:3
            }
        }
    });
   
});
$(document).ready(function () {
    'use strict';
    $(".recommand").change(function (){
        if($("input#yes3").is(':checked')){
            $(".copy").slideDown();
             $('.whyNo').slideUp();
        }else if("input#no3") {
            $(".copy").slideUp();
            $('.whyNo').slideDown();
        }
    });
});
/* Add active  class in sunmenu and menu  */
$(document).ready(function () {
    'use strict';
    
    $('.closeMenu').click(function (){
        $('.navMobile').addClass('closeM').removeClass('open');
    });
    $('.openMenu').click(function () {
        $('.navMobile').addClass('open').removeClass('closeM');
    });
    $("nav li.hasMenu").click(function () {
        $(this).toggleClass("open");
    });
    
    $(window).click(function() {
        $("nav li.hasMenu").removeClass("open");
        $('.navMobile').addClass('closeM').removeClass('open');
    });
    
    $('nav li.hasMenu, .sub-menu , .navbarM, .openMenu').click(function(event){
        event.stopPropagation();
    });

  

});
//========================Navbar=====================//
$(document).ready(function (){
    
    
   
  $("nav li a, .navMobile li a").click(function (){
      
      
        $("html, body").animate({
            scrollTop: $("#"+ $(this).data("scroll")).offset().top - 60
        }, 1000);
      //Add Class Active
      $(this).addClass("active").parent().siblings().find("a").removeClass("active");
   });
    // Sycn Navbar
    
    $(window).scroll(function (){
        $("section").each(function (){
            if($(window).scrollTop() > ($(this).offset().top - 50)){
                sectionID=$(this).attr("id");
                $("nav li a , .navMobile li a").removeClass("active");
                $("nav li a[data-scroll='"+ sectionID +"'], .navMobile li a[data-scroll='"+ sectionID +"']").addClass("active");
            }
        })
    })
});

//========================WOW=====================//
// Init WOW.js and get instance
     wow = new WOW({
         boxClass:     'wow',      
         animateClass: 'animated', 
         offset:      100,          
         mobile:       true,       
         live:         true        
     });
    wows = new WOW({
         boxClass:     'wows',      
         offset:      250,          
         mobile:       false,       
         live:         false        
     });
wow.init();
wows.init();

//========================Scroll To Top=====================//
$(document).ready(function (){

    var scrollButton=$("#scroll");

    $(window).scroll(function(){
        if($(this).scrollTop()>=700)
        {
            scrollButton.show();
        }
        else
        {
            scrollButton.hide();
        }
    });
    scrollButton.click(function(){
        $("html,body").animate({scrollTop:0},900);
    });
});
jQuery('.mm-prev-btn').hide();

	var x;
	var count;
	var current;
	var percent;
	var z = [];

	init();
	getCurrentSlide();
	goToNext();
	goToPrev();
	getCount();
//	 checkStatus();
//	 buttonConfig();
	buildStatus();
	deliverStatus();
	submitData();
	goBack();

	function init() {
		
		jQuery('.mm-survey-container .mm-survey-page').each(function() {

			var item;
			var page;

			item = jQuery(this);
			page = item.data('page');

			item.addClass('mm-page-'+page);
			//item.html(page);

		});

	}

	function getCount() {

		count = jQuery('.mm-survey-page').length;
		return count;

	}

	function goToNext() {

		jQuery('.mm-next-btn').on('click', function() {
			goToSlide(x);
			getCount();
			current = x + 1;
			var g = current/count;
			buildProgress(g);
			var y = (count + 1);
			getButtons();
			jQuery('.mm-survey-page').removeClass('active');
			jQuery('.mm-page-'+current).addClass('active');
			getCurrentSlide();
			checkStatus();
			if( jQuery('.mm-page-'+count).hasClass('active') ){
				if( jQuery('.mm-page-'+count).hasClass('pass') ) {
					jQuery('.mm-finish-btn').addClass('active');
				}
				else {
					jQuery('.mm-page-'+count+' .mm-survery-content .mm-survey-item').on('click', function() {
						jQuery('.mm-finish-btn').addClass('active');
					});
				}
			}
			else {
				jQuery('.mm-finish-btn').removeClass('active');
				if( jQuery('.mm-page-'+current).hasClass('pass') ) {
					jQuery('.mm-survey-container').addClass('good');
					jQuery('.mm-survey').addClass('okay');
				}
				else {
					jQuery('.mm-survey-container').removeClass('good');
					jQuery('.mm-survey').removeClass('okay');
				}
			}
			buttonConfig();
		});

	}

	function goToPrev() {

		jQuery('.mm-prev-btn').on('click', function() {
			goToSlide(x);
			getCount();			
			current = (x - 1);
			var g = current/count;
			buildProgress(g);
			var y = count;
			getButtons();
			jQuery('.mm-survey-page').removeClass('active');
			jQuery('.mm-page-'+current).addClass('active');
			getCurrentSlide();
			checkStatus();
			jQuery('.mm-finish-btn').removeClass('active');
			if( jQuery('.mm-page-'+current).hasClass('pass') ) {
				jQuery('.mm-survey-container').addClass('good');
				jQuery('.mm-survey').addClass('okay');
			}
			else {
				jQuery('.mm-survey-container').removeClass('good');
				jQuery('.mm-survey').removeClass('okay');
			}
			buttonConfig();
		});

	}

	function buildProgress(g) {

		if(g > 1){
			g = g - 1;
		}
		else if (g === 0) {
			g = 1;
		}
		g = g * 100;
		jQuery('.mm-survey-progress-bar').css({ 'width' : g+'%' });

	}

	function goToSlide(x) {

		return x;

	}

	function getCurrentSlide() {

		jQuery('.mm-survey-page').each(function() {

			var item;

			item = jQuery(this);

			if( jQuery(item).hasClass('active') ) {
				x = item.data('page');
			}
			else {
				
			}

			return x;

		});

	}

	function getButtons() {

		if(current === 0) {
			current = y;
		}
		if(current === count) {
			jQuery('.mm-next-btn').hide();
		}
		else if(current === 1) {
			jQuery('.mm-prev-btn').hide();
		}
		else {
			jQuery('.mm-next-btn').show();
			jQuery('.mm-prev-btn').show();
		}

	}

	jQuery('.mm-survey-q li input').each(function() {

		var item;
		item = jQuery(this);

		jQuery(item).on('click', function() {
			if( jQuery('input:checked').length > 0 ) {
		    	// console.log(item.val());
		    	jQuery('label').parent().removeClass('active');
		    	item.closest( 'li' ).addClass('active');
			}
			else {
				//
			}
		});

	});

	percent = (x/count) * 100;
	jQuery('.mm-survey-progress-bar').css({ 'width' : percent+'%' });

	function checkStatus() {
		jQuery('.mm-survery-content .mm-survey-item').on('click', function() {
			var item;
			item = jQuery(this);
			item.closest('.mm-survey-page').addClass('pass');
		});
	}

	function buildStatus() {
		jQuery('.mm-survery-content .mm-survey-item').on('click', function() {
            
                var item;
                item = jQuery(this);
                item.addClass('bingo');
                item.closest('.mm-survey-page').addClass('pass');
                jQuery('.mm-survey-container').addClass('good');
                if(jQuery('input.mm_name').val() == '' || jQuery('input.mm_phone').val()  == ''){
                   jQuery('.mm-next-btn').hide();
                }else {
                    jQuery('.mm-next-btn').show();
                }
                
            
		});
	}

	function deliverStatus() {
		jQuery('.mm-survey-item').on('click', function() {
            
			if( jQuery('.mm-survey-container').hasClass('good') ){
				jQuery('.mm-survey').addClass('okay');
			}
			else {
				jQuery('.mm-survey').removeClass('okay');	
			}
			buttonConfig();
            
		});
	}

	function lastPage() {
		if( jQuery('.mm-next-btn').hasClass('cool') ) {
			alert('cool');
		}
	}

	function buttonConfig() {
		if( jQuery('.mm-survey').hasClass('okay') ) {
			jQuery('.mm-next-btn button').prop('disabled', false);
		}
		else {
			jQuery('.mm-next-btn button').prop('disabled', true);
		}
	}

	function submitData() {
		jQuery('.mm-finish-btn').on('click', function() {
//			collectData();
			jQuery('.mm-survey-bottom').slideUp();
			jQuery('.mm-survey-results').slideDown();
		});
	}

	function collectData() {
		
		var map = {};
		var ax = ['0','red','mercedes','3.14','3'];
		var answer = '';
		var total = 0;
		var ttl = 0;
		var g;
		var c = 0;

		jQuery('.mm-survey-item input:checked').each(function(index, val) {
			var item;
			var data;
			var name;
			var n;

			item = jQuery(this);
			data = item.val();
			name = item.data('item');
			n = parseInt(data);
			total += n;

			map[name] = data;

		});

		jQuery('.mm-survey-results-container .mm-survey-results-list').html('');

		for (i = 1; i <= count; i++) {

			var t = {};
			var m = {};
			answer += map[i] + '<br>';
			
//			if( map[i] === ax[i]) {
//				g = map[i];
//				p = 'correct';
//				c = 1;
//			}
//			else {
//				g = map[i];
//				p = 'incorrect';
//				c = 0;
//			}
            if( map[i] === ax[i]) {
				g = map[i];
				c = 1;
			}
			else {
				g = map[i];
				c = 0;
			}

			

		}

	
		jQuery('.mm-survey-results-score').html( "تم الارسال بنجاح" );

	}

	function goBack() {
		jQuery('.mm-back-btn').on('click', function() {
			jQuery('.mm-survey-bottom').slideDown();
			jQuery('.mm-survey-results').slideUp();
		});
	}
   
//========================Load=====================//
$(document).ready(function () {
    "use strict";
    $(window).on('load',function () {
        $(".load").fadeOut(100, function () {
            $("body").css("overflow", "auto");
            $(this).fadeOut(100, function () {
                $(this).remove();
            });
        });
    });

});

/* share */
function copyToClipboard(element) {
    $('.copyToClipboard').slideDown();
    
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}
       
